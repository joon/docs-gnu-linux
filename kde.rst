=====
 KDE
=====

Restart Plasma Desktop
======================

.. code-block:: sh

   $ kbuildsycoca4 && kquitapp plasma-desktop && kstart plasma-desktop


Klipper shortcut settings
=========================

KDE 4.10.0-1.2.1

If you do it in Global Keyboard Shortcuts, it will be reset on logout.

https://bugs.kde.org/show_bug.cgi?id=300532#c1

- right click on system tray expander arrow 
- select "System Tray Settings" 
- in "Entries" list, change keyboard shortcut for Klipper

