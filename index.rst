.. Docs: GNU/Linux documentation master file, created by
   sphinx-quickstart on Mon Apr 15 10:13:07 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Docs: GNU/Linux's documentation!
===========================================

Contents:

.. toctree::
   :maxdepth: 1

   system
   packages
   kde
   EPD
   pyqt
   virtualenv
   backup

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

