PyQT Installation
=================

Need to install sip and PyQT

Make sure you have ``libQtWebKit-devel`` package before you install PyQT to
get the ``QtWebKit`` installed properly.

I still does not know how to fix the following error:

.. code-block:: python

   >>> from PyQt4.QtNetwork import QNetworkCookie, QNetworkAccessManager

   ImportError: /home/joon/virtualenvs/epd/lib/python2.7/site-packages/PyQt4/QtNetwork.so: undefined symbol: _ZTI14QHttpMultiPart

Dependencies
------------

* make
* gcc
* gcc-c++
* gcc-fortran
* libqt4-devel
* libqtwebkit-devel
* libQtNetworkManager-devel

.. code-block:: sh

   $ sudo zypper in make gcc gcc-c++ gcc-fortran libqt4-devel libqtwebkit-devel libQtNetworkManager-devel


Download
--------

The link to the download pages for both sip and PyQT4 are at:
http://pyqt.sourceforge.net/Docs/PyQt4/installation.html

.. note:: The installation fails if you do the following in the NTFS external
          drive such as a USB stick. Extract the source into a directory in
          ~ first.

Set the environmental variable ``PYTHON``
-----------------------------------------

.. code-block:: sh

   $ PYTHON=$EPD
   $ PYTHON=$anaconda

Sip
---

Remove previous installation
++++++++++++++++++++++++++++

.. code-block:: sh 

   $ rm $PYTHON/lib/python2.7/site-packages/sip*

Configure
+++++++++

.. code-block:: sh 

   $ $PYTHON/bin/python configure.py


make & make install
+++++++++++++++++++

.. code-block:: sh 
   
   $ make -j $(expr $(nproc) + 1) && make install

PyQT4
-----

Remove previous installation
++++++++++++++++++++++++++++

.. code-block:: sh 
   
   $ sudo rm -rf $PYTHON/lib/python2.7/site-packages/PyQt4

Configure
+++++++++

.. code-block:: sh 

   $ $PYTHON/bin/python configure.py

make & make install
+++++++++++++++++++

.. code-block:: sh 

   $ make -j $(expr $(nproc) + 1)
   $ sudo make install
