Enthought Python Distribution (EPD) Installation
================================================

Download
--------

EPD repo login page:
https://www.enthought.com/accounts/login/?next=/repo/epd/

Set the environmental variable::

   $ PYTHON=$EPD

Update Packages
---------------

Manual
------

.. code-block:: sh

   $ $PYTHON/bin/easy_install pip


Using enpkg
~~~~~~~~~~~

.. code-block:: sh

   $ $PYTHON/bin/enpkg matplotlib pandas scipy


Using pip
~~~~~~~~~

.. code-block:: sh

   $ $PYTHON/bin/pip install bottleneck


Manual updates
~~~~~~~~~~~~~~

IPython
+++++++

.. code-block:: sh

   $ rm -rf $PYTHON/lib/python2.7/site-packages/IPython $PYTHON/lib/python2.7/site-packages/ipython*
   $ rm -rf build
   $ $PYTHON/bin/python setup.py install

Cython
++++++

.. code-block:: sh

   $ rm -rf $PYTHON/lib/python2.7/site-packages/Cython* $PYTHON/lib/python2.7/site-packages/cython.*
   $ rm -rf build
   $ $PYTHON/bin/python setup.py install


CythonGSL
+++++++++

.. code-block:: sh

   $ rm -rf build
   $ $PYTHON/bin/python setup.py install


Statsmodels
+++++++++++

.. code-block:: sh

   $ rm -rf $PYTHON/lib/python2.7/site-packages/statsmodels* 
   $ $PYTHON/bin/pip install patsy

   $ git clone git@github.com:joonro/statsmodels.git
   $ cd statsmodels
   $ $PYTHON/bin/python setup.py install

